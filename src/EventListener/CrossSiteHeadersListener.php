<?php
/**
 * Created by PhpStorm.
 * User: pico
 * Date: 15/12/2019
 * Time: 19:38
 */

namespace App\EventListener;


use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

class CrossSiteHeadersListener
{
    public function onKernelResponse(FilterResponseEvent $event)
    {
        $headers = [
            "Access-Control-Allow-Origin" => "*",
            "Access-Control-Allow-Methods" => "POST, GET, PUT, DELETE",
            'Access-Control-Allow-Headers' => 'Content-Type, x-wsse',
            'Access-Control-Max-Age' => 86400,
            'Content-Type' => 'application/json'
        ];
        $response = $event->getResponse();
        $response->headers->add($headers);

    }
}