<?php
/**
 * Created by PhpStorm.
 * User: pico
 * Date: 13/12/2019
 * Time: 15:53
 */

namespace App\Util;


trait DoctrinePagerHelper
{
    public function getPage($page = 1)
    {
        if ($page < 1) {
            $page = 1;
        }

        return intval($page);
    }

    public function getLimit($limit = 20)
    {
        if ($limit < 1) {
            $limit = 1;
        }

        return intval($limit);
    }

    public function getOffset($page, $limit)
    {
        $offset = 0;
        if ($page != 0 && $page != 1) {
            $offset = ($page - 1) * $limit;
        }

        return $offset;
    }
}