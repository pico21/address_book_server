<?php
/**
 * Created by PhpStorm.
 * User: pico
 * Date: 15/12/2019
 * Time: 13:35
 */

namespace App\Security\Authentication;


use App\Entity\User;
use App\Security\Authentication\Token\WsseUserToken;

class WsseService
{
    public function generateToken(User $user){

        $password = $user->getPassword();
        $token = new WsseUserToken();

        $token->created = (new \DateTime())->format('Y-m-d H:i:s');
        $token->nonce   = $this->getNonce();
        $token->digest  = base64_encode(sha1(base64_decode($token->nonce).$token->created.$password, true));
        return $token;
    }

    protected function getNonce(): string
    {
       return base64_encode( bin2hex( openssl_random_pseudo_bytes( 16 ) ) );
    }

}