<?php
/**
 * Created by PhpStorm.
 * User: pico
 * Date: 15/12/2019
 * Time: 13:33
 */

namespace App\Controller;


use App\Entity\User;
use App\Security\Authentication\WsseService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

;


/**
 * @Route("/auth",name="api_auth_")
 */
class LoginController extends AbstractFOSRestController
{

    /**
     * Login User
     * @Rest\Options("/login")
     *
     */
    public function login_check(Request $request){

        return $this->handleView($this->view([],Response::HTTP_OK) );
    }

    /**
    * Login User
    * @Rest\Post("/login")
    *
    */
    public function login(Request $request, WsseService $service, UserManagerInterface $manager){
        $body = $request->getContent();
        $json = json_decode($body , true);

        $username = $json["username"];
        $password = $json["password"];

        $user = $manager->findUserByUsernameOrEmail($username);

        if (!$user){
            $err = ["status" => "error",
                    "description" => "user and password are wrong"];
            return $this->handleView($this->view(["status" => "err", "description" => "Username or Password not valid."],  Response::HTTP_UNAUTHORIZED));
        }

        $defaultEncoder = new MessageDigestPasswordEncoder('sha512', true, 5000);

        $salt = $user->getSalt();

        if(!$defaultEncoder->isPasswordValid($user->getPassword(), $password, $salt)) {
            return $this->handleView($this->view(["status" => "err", "description" => "Username or Password not valid."],  Response::HTTP_UNAUTHORIZED));
        }

        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));
//
//        $event = new InteractiveLoginEvent($request, $token);
//        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);


        $token = $service->generateToken($user);

        $out = [
            "Username"          => $user->getUsername(),
            "PasswordDigest"    => $token->digest,
            "Nonce"             => $token->nonce,
            "Created"           => $token->created

        ];

//        dump('UsernameToken Username="'.$user->getUsername().'", PasswordDigest="'.$token->digest.'", Nonce="'.$token->nonce.'", Created="'.$token->created.'"');
        return $this->handleView($this->view(["status" => "ok", "credentials" => $out], Response::HTTP_OK));
    }
}