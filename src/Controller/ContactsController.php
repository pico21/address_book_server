<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Entity\EmailContact;
use App\Entity\PhoneContact;
use App\Entity\Tag;
use App\Form\ContactType;
use App\Repository\ContactRepository;
use App\Service\ContactService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;;

/**
 * @Route("/api",name="api_")
 */
class ContactsController extends AbstractFOSRestController
{


    /**
     * @Rest\Options("/contacts")
     *
     */
    public function options(Request $request){

        return $this->handleView($this->view([],Response::HTTP_OK) );
    }

   /**
    * Find Contacts
    * @Rest\Get("/contacts")
    *
    */
   public function getContacts(Request $request, ContactService $service){
       /**
        * @var ContactRepository $repository
        */
       $repository = $this->getDoctrine()->getRepository(Contact::class);

       $firstname    = $request->get("firstname");
       $lastname     = $request->get("lastname");
       $tag          = $request->get("tag");
       $email        = $request->get("email");
       $phone        = $request->get("phone");
       $social       = $request->get("social");
       $sorts        = $request->get("sort");
       $search       = $request->get("search");
       $page         = $request->get("page", 1);
       $limit        = $request->get("limit", 10);

       $contacts = $service->searchContact($firstname, $lastname, $tag, $email, $phone, $social, $search, $sorts, $page, $limit);
       $total = $service->getTotalofContacts($firstname, $lastname, $tag, $email, $phone, $search);

       //TODO renderlo più efficace
       return $this->handleView($this->view(["Contacts" => $contacts, "total" => $total]));
   }

    /**
     * Find Contact
     * @Rest\Get("/contacts/{id<\d+>}")
     *
     */
   public function getContact(int $id){
       $repository = $this->getDoctrine()->getRepository(Contact::class);

       if ($id) {
           $contact = $repository->find($id);
           return $this->handleView($this->view($contact));
       }

       return $this->handleView($this->view(["status" => "error", "description" => "Contact not found"]), Response::HTTP_NOT_FOUND);
   }

   /**
    * Create Contact
    * @Rest\Post("/contacts")
    */
    public function postContact(Request $request, ContactService $service){
        $data = json_decode($request->getContent(), true);
        if (!isset($data["Contacts"])){
            //TODO generate error
            return $this->handleView($this->view(["status" => "error", "description" => "Contacts not found"]), Response::HTTP_BAD_REQUEST);
        }
        /**
         * in $force_contact saranno salvati tutti i contact che dovranno ricevere una conferma prima dell'inserimento
         */
        $force_contact = [];
        $contacts = [];
        $entityManager = $this->getDoctrine()->getManager();
        foreach ($data["Contacts"] as $contact_json){

            /**
             * FIND FISTNAME LASTNAME
             */
            $contact = $service->createContactFromArray($contact_json);
            if ($contact) {
                $force = isset($contact_json["force"]) ? true : false;
                if ($service->isExistContact($contact->getFirstname(), $contact->getLastname()) && !$force){
                    $force_contact[] = clone $contact;
                    continue;
                }

                $entityManager->persist(clone $contact);
                $contacts[] = $contact;
            }
        }
        $entityManager->flush();

        return $this->handleView($this->view(["contacts" => $contacts,"force" => $force_contact]), Response::HTTP_CREATED);
    }

    /**
     * @Rest\Delete("/contacts")
     */
    public function deleteContacts(Request $request){
        $repository = $this->getDoctrine()->getRepository(Contact::class);
        $data = json_decode($request->getContent(), true);
        if (!isset($data["Contacts"])){
            //TODO generate error
            return $this->handleView($this->view(["status" => "error", "description" => "Contacts not found"]), Response::HTTP_BAD_REQUEST);
        }
        $em = $this->getDoctrine()->getManager();
        $err = [];
        foreach ($data["Contacts"] as $contact_json) {
            if (!isset($contact_json["id"])){
                $err[] = $contact_json["id"];
                continue;
            }
            $contact = $repository->find($contact_json["id"]);
            if (!$contact) {
                $err[] = $contact_json["id"];
            }

            $em->remove($contact);


        }
        $em->flush();

        return $this->handleView($this->view(["status" => "ok", "description" => "Contact removed", "errors" => $err]));
    }

    /**
     * @Rest\Delete("/contacts/{id<\d+>}")
     */
    public function deleteContact(int $id){
        $repository = $this->getDoctrine()->getRepository(Contact::class);

        if ($id) {
            $contact = $repository->find($id);
            if (!$contact)
                return $this->handleView($this->view(["status" => "error", "description" => "Contact not found"]), Response::HTTP_NOT_FOUND);
            $em = $this->getDoctrine()->getManager();
            $em->remove($contact);
            $em->flush();
            return $this->handleView($this->view(["status" => "ok", "description" => "Contact removed"]));
        }

        return $this->handleView($this->view(["status" => "error", "description" => "Contact not found"]), Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Rest\Put("/contacts")
     */
    public function updateContact(Request $request, ContactService $service){
        $repository = $this->getDoctrine()->getRepository(Contact::class);
        $data = json_decode($request->getContent(), true);
        if (!isset($data["Contacts"])){
            //TODO generate error
            return $this->handleView($this->view(["status" => "error", "description" => "Contacts not found"]), Response::HTTP_BAD_REQUEST);
        }

        $contacts = [];
        $entityManager = $this->getDoctrine()->getManager();
        foreach ($data["Contacts"] as $contact_json){
            $contact = $service->updateContactFromArray($contact_json);
            if ($contact) {
                $entityManager->persist(clone $contact);
                $contacts[] = $contact;
            }
        }
        $entityManager->flush();
        return $this->handleView($this->view(["contacts" => $contacts]), Response::HTTP_CREATED);
    }
}


