<?php

namespace App\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;;


class MainController extends AbstractFOSRestController
{
    /**
     * @Route("/",name="home")
     */
    public function index()
    {
        return $this->handleView($this->view([]));
    }
}
