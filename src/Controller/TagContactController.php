<?php

namespace App\Controller;

use App\Entity\Tag;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;;


class TagContactController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("/api/tags_wall")
     */
    public function tags_wall()
    {
        $repository = $this->getDoctrine()->getRepository(Tag::class);
        $tags = $repository->findAll();
        return $this->handleView($this->view($tags));
    }
}
