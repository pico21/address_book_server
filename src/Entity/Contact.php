<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContactRepository")
 */
class Contact
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag", inversedBy="contacts", cascade={"persist"})
     */
    private $tags;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EmailContact", mappedBy="contact",cascade={"persist"})
     */
    private $emails;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PhoneContact", mappedBy="contact",cascade={"persist"})
     */
    private $phones;

    /**
     *  @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $add_date;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SocialContact", mappedBy="contact",cascade={"persist"})
     */
    private $socials;

    public function __construct()
    {
        $this->tags     = new ArrayCollection();
        $this->emails   = new ArrayCollection();
        $this->phones   = new ArrayCollection();
        $this->add_date = new \DateTime();
        $this->socials = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
        }

        return $this;
    }
    

    /**
     * @return Collection|EmailContact[]
     */
    public function getEmails(): Collection
    {
        return $this->emails;
    }

    public function addEmail(EmailContact $email): self
    {
        if (!$this->emails->contains($email)) {
            $this->emails[] = $email;
            $email->setContact($this);
        }

        return $this;
    }

    public function removeEmail(EmailContact $email): self
    {
        if ($this->emails->contains($email)) {
            $this->emails->removeElement($email);
            // set the owning side to null (unless already changed)
            if ($email->getContact() === $this) {
                $email->setContact(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PhoneContact[]
     */
    public function getPhones(): Collection
    {
        return $this->phones;
    }

    public function addPhone(PhoneContact $phone): self
    {
        if (!$this->phones->contains($phone)) {
            $this->phones[] = $phone;
            $phone->setContact($this);
        }

        return $this;
    }

    public function removePhone(PhoneContact $phone): self
    {
        if ($this->phones->contains($phone)) {
            $this->phones->removeElement($phone);
            // set the owning side to null (unless already changed)
            if ($phone->getContact() === $this) {
                $phone->setContact(null);
            }
        }

        return $this;
    }

    public function getAddDate(): ?\DateTimeInterface
    {
        return $this->add_date;
    }

    public function setAddDate(\DateTimeInterface $add_date): self
    {

        $this->add_date = $add_date ?? (new \DateTime());

        return $this;
    }

    /**
     * @return Collection|SocialContact[]
     */
    public function getSocials(): Collection
    {
        return $this->socials;
    }

    public function addSocial(SocialContact $social): self
    {
        if (!$this->socials->contains($social)) {
            $this->socials[] = $social;
            $social->setContact($this);
        }

        return $this;
    }

    public function removeSocial(SocialContact $social): self
    {
        if ($this->socials->contains($social)) {
            $this->socials->removeElement($social);
            // set the owning side to null (unless already changed)
            if ($social->getContact() === $this) {
                $social->setContact(null);
            }
        }

        return $this;
    }

}
