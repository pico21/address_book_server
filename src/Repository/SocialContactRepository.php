<?php

namespace App\Repository;

use App\Entity\SocialContact;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method SocialContact|null find($id, $lockMode = null, $lockVersion = null)
 * @method SocialContact|null findOneBy(array $criteria, array $orderBy = null)
 * @method SocialContact[]    findAll()
 * @method SocialContact[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SocialContactRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SocialContact::class);
    }

    // /**
    //  * @return SocialContact[] Returns an array of SocialContact objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SocialContact
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
