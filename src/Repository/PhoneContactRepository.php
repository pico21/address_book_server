<?php

namespace App\Repository;

use App\Entity\PhoneContact;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PhoneContact|null find($id, $lockMode = null, $lockVersion = null)
 * @method PhoneContact|null findOneBy(array $criteria, array $orderBy = null)
 * @method PhoneContact[]    findAll()
 * @method PhoneContact[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PhoneContactRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PhoneContact::class);
    }

    // /**
    //  * @return PhoneContact[] Returns an array of PhoneContact objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PhoneContact
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
