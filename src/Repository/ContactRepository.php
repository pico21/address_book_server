<?php

namespace App\Repository;

use App\Entity\Contact;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * @method Contact|null find($id, $lockMode = null, $lockVersion = null)
 * @method Contact|null findOneBy(array $criteria, array $orderBy = null)
 * @method Contact[]    findAll()
 * @method Contact[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Contact::class);
    }


    public function searchContactsCount($criteri = [], $search = [], $orders = [])
    {
        $obj = $this->createQueryBuilder('c')
            ->select('COUNT(c)')
            ->leftJoin("c.tags", "t")
            ->leftJoin("c.phones", "p")
            ->leftJoin("c.emails", "e")
            ->leftJoin("c.socials", "s");

        /**
         * filter
         */
        if ($criteri)
            foreach ($criteri as $key => $criterio){
                $column = $this->getColumnHelper($key);
                $obj->andWhere("$column = :{$key}_criterio")
                    ->setParameter("{$key}_criterio","$criterio");
            }

        /**
         * search
         */

        $orX = null;
        if ($search) {
            $orX = $obj->expr()->orX();
            foreach ($search as $key => $value) {
                $column = $this->getColumnHelper($key);
                $orX->add($obj->expr()->like($column, ":{$key}_search"));
                $obj->setParameter("{$key}_search","%$value%");
            }
            $obj->andWhere($orX);
        }

        /**
         * order
         */
        if ($orders)
            foreach ($orders as $key => $value){
                $column = $this->getColumnHelper($key);
                $obj->orderBy("$column", "$value");
            }
        return $obj
            ->getQuery()
            ->getResult(Query::HYDRATE_SINGLE_SCALAR)
            ;
    }

     /**
      * @return Contact[] Returns an array of Contact objects
      */

    public function searchContacts($criteri = [], $search = [], $orders = [], int $offset = 0, int $limit = 10)
    {
        $obj = $this->createQueryBuilder('c')
                ->leftJoin("c.tags", "t")
                ->leftJoin("c.phones", "p")
                ->leftJoin("c.emails", "e")
                ->leftJoin("c.socials", "s");

        /**
         * filter
         */
        if ($criteri)
            foreach ($criteri as $key => $criterio){
                if ($criterio) {
                    $column = $this->getColumnHelper($key);
                    $obj->andWhere("$column = :{$key}_criterio")
                        ->setParameter("{$key}_criterio", "$criterio");
                }
            }

        /**
         * search
         */

        $orX = null;
        if ($search) {
            $orX = $obj->expr()->orX();
            foreach ($search as $key => $value) {
                $column = $this->getColumnHelper($key);
                $orX->add($obj->expr()->like($column, ":{$key}_search"));
                $obj->setParameter("{$key}_search","%$value%");
            }
            $obj->andWhere($orX);
        }

        /**
         * order
         */
        if ($orders)
            foreach ($orders as $key => $value){
                $column = $this->getColumnHelper($key);
                $obj->orderBy("$column", "$value");
            }

        return
            $obj
                ->addGroupBy("c.id")
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getQuery()
                ->getResult()
            ;
    }

    private function getColumnHelper($key){
        switch ($key){
            case "tag":
                $column = "t.name";
                break;
            case "email":
                $column = "e.email";
                break;
            case "phone":
                $column = "p.number";
                break;
            case "social":
                $column = "s.type";
                break;
            default:
                $column = "c.$key";
        }
        return $column;
    }


    /*
    public function findOneBySomeField($value): ?Contact
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
