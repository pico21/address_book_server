<?php
/**
 * Created by PhpStorm.
 * User: pico
 * Date: 13/12/2019
 * Time: 14:37
 */

namespace App\Service;


use App\Entity\Contact;
use App\Entity\EmailContact;
use App\Entity\PhoneContact;
use App\Entity\SocialContact;
use App\Entity\Tag;
use App\Repository\ContactRepository;

use App\Util\DoctrinePagerHelper;
use Doctrine\ORM\EntityManagerInterface;

class ContactService{
    use DoctrinePagerHelper;
    private $entity_manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->entity_manager = $manager;
    }

    public function isExistContact($firstname = null, $lastname = null, $tag = null, $email = null, $phone = null, $social = null):bool{
        /**
         * @var ContactRepository $repository
         */
        $repository = $this->entity_manager->getRepository(Contact::class);
        $criteri = [];
        if ($firstname) $criteri["firstname"] = $firstname;
        if ($lastname)  $criteri["lastname"]  = $lastname;
        if ($tag)       $criteri["tag"]       = $tag;
        if ($email)     $criteri["email"]     = $email;
        if ($phone)     $criteri["phone"]     = $phone;
        if ($social)    $criteri["social"]    = $social;

        $result = $repository->searchContacts($criteri, [], [], 0, 1);

        if (!$result)
            return false;
        return true;
    }

    public function searchContact($firstname = null, $lastname = null, $tag = null, $email = null, $phone = null, $social = null, $search = null, $sorts = null, int $page = 0, int $limit = 10){

        /**
         * Sort
         * es. firstname_asc
         *     lastname_desc
         */
        $order = [];
        if ($sorts){
            if (!is_array($sorts))
                $sorts = [$sorts];

            foreach ($sorts as $sort){
                $tmp = explode("_", $sort);
                if (isset($tmp[1]))
                    $order[$tmp[0]] = $tmp[1];
                else
                    $order[$tmp[0]] = "asc";
            }
        }

        /**
         * Search
         * find anything to anything
         */
        $args_to_search = [];
        if ($search){
            $args_to_search["firstname"] = $search;
            $args_to_search["lastname"]  = $search;
            $args_to_search["tag"]       = $search;
            $args_to_search["email"]     = $search;
            $args_to_search["phone"]     = $search;
            $args_to_search["social"]    = $search;
        }
        /**
         * Filter
         */
        $args_to_filter = [];
        if ($firstname)
            $args_to_filter["firstname"] = $firstname;
        if ($lastname)
            $args_to_filter["lastname"] = $lastname;
        if ($tag)
            $args_to_filter["tag"] = $tag;
        if ($email)
            $args_to_filter["email"] = $email;
        if ($phone)
            $args_to_filter["phone"] = $phone;
        if ($social)
            $args_to_filter["social"] = $social;
        /**
         * @var ContactRepository $repository
         */
        $repository = $this->entity_manager->getRepository(Contact::class);
        $page = $this->getPage($page);
        $limit = $this->getLimit($limit);
        $offset = $this->getOffset($page, $limit);

        $result = $repository->searchContacts($args_to_filter, $args_to_search, $order, $offset, $limit);
        if (!$result){
            $total = $repository->searchContactsCount($args_to_filter, $args_to_search, $order);

            $result = $repository->searchContacts($args_to_filter, $args_to_search, $order, $total, $limit);
        }
        return $result;
    }

    public function getTotalofContacts($firstname = null, $lastname = null, $tag = null, $email = null, $phone = null, $social = null, $search = null){
        /**
         * Sort
         * es. firstname_asc
         *     lastname_desc
         */

        /**
         * Search
         * find anything to anything
         */
        $args_to_search = [];
        if ($search){
            $args_to_search["firstname"] = $search;
            $args_to_search["lastname"]  = $search;
            $args_to_search["tag"]       = $search;
            $args_to_search["email"]     = $search;
            $args_to_search["phone"]     = $search;
            $args_to_search["social"]    = $search;
        }
        /**
         * Filter
         */
        $args_to_filter = [];
        if ($firstname)
            $args_to_filter["firstname"] = $firstname;
        if ($lastname)
            $args_to_filter["lastname"] = $lastname;
        if ($tag)
            $args_to_filter["tag"] = $tag;
        if ($email)
            $args_to_filter["email"] = $email;
        if ($phone)
            $args_to_filter["phone"] = $phone;
        if ($social)
            $args_to_filter["social"] = $social;
        /**
         * @var ContactRepository $repository
         */
        $repository = $this->entity_manager->getRepository(Contact::class);

        return $repository->searchContactsCount($args_to_filter, $args_to_search, []);
    }

    public function updateContactFromArray(array $arr, bool $force_create = false):?Contact{
        $repo = $this->entity_manager->getRepository(Contact::class);
        /**
         * @var Contact $contact
         */
        $contact = $repo->find($arr['id']);
        if (!$contact){
            if ($force_create)
                return $this->createContactFromArray($arr);
            // TODO implement ContactException
            return null;
        }

        $contact->setFirstname($arr["firstname"]);
        $contact->setLastname($arr["lastname"]);
        if (isset($arr["emails"])) {
            foreach ($arr["emails"] as $email) {
                $emailContact = new EmailContact();
                $emailContact->setEmail($email);
                $contact->addEmail($emailContact);
            }
        }
        if (isset($arr["phones"])) {
            foreach ($arr["phones"] as $phone) {
                $phoneContact = new PhoneContact();
                $phoneContact->setNumber($phone);
                $contact->addPhone($phoneContact);
            }
        }
        if (isset($arr["tags"])) {
            foreach ($arr["tags"] as $tag) {
                $tagContact = new Tag();
                $tagContact->setName($tag);
                $contact->addTag($tagContact);
            }
        }

        if (isset($arr["socials"])) {
            foreach ($arr["socials"] as $social) {
                $socialContact = new SocialContact();
                $socialContact->setType($social["type"]);
                $socialContact->setProfileLink($social["profile_link"]);
                $contact->addSocial($socialContact);
            }
        }
        return $contact;
    }

    public function createContactFromArray(array $arr):Contact
    {
        $contact = new Contact();
        $contact->setFirstname($arr["firstname"]);
        $contact->setLastname($arr["lastname"]);
        if (isset($arr["emails"])) {
            foreach ($arr["emails"] as $email) {
                $emailContact = new EmailContact();
                $emailContact->setEmail($email);
                $contact->addEmail($emailContact);
            }
        }
        if (isset($arr["phones"])) {
            foreach ($arr["phones"] as $phone) {
                $phoneContact = new PhoneContact();
                $phoneContact->setNumber($phone);
                $contact->addPhone($phoneContact);
            }
        }
        if (isset($arr["tags"])) {
            foreach ($arr["tags"] as $tag) {
                $tagContact = new Tag();
                $tagContact->setName($tag);
                $tagContact->setWeight(1);
                $contact->addTag($tagContact);
            }
        }

        if (isset($arr["socials"])) {
            foreach ($arr["socials"] as $social) {
                $socialContact = new SocialContact();
                $socialContact->setType($social["type"]);
                $socialContact->setProfileLink($social["profile_link"]);
                $contact->addSocial($socialContact);
            }
        }

        return $contact;
    }

}